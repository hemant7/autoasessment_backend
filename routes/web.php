<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'ExampleController@GetQuestionsStep1');
$router->post('post/formstart', 'ExampleController@formstart');
$router->post('post/stepone', 'ExampleController@PostQuestionsStep1');
$router->get('/step2', 'ExampleController@GetQuestionsStep2');
$router->post('step2/post/steptwo', 'ExampleController@PostQuestionsStep2');
$router->get('/step3', 'ExampleController@GetQuestionsStep3');
$router->post('step3/post/stepthree', 'ExampleController@PostQuestionsStep3');
$router->post('comment/mail/post/result', 'ExampleController@commentsSend');



// Route::resource('/', 'ExampleController@GetQuestions');