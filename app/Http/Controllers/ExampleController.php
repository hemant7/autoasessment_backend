<?php

namespace App\Http\Controllers;
use App\Questions;
use App\Answers;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Response;
// use Illuminate\Mail\Mailer;
// use Mail;
use Illuminate\Support\Facades\Mail;


class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function formstart(Request $request){

        // dd($request->all());
      $answers = new User;
      $answers->user_id =rand(1000,9999);
      $answers->size = $request->get("size");
      $answers->country = $request->get("country");
      $answers->save();

      $data["status"] = "success";
      $data["user"] = $answers->user_id;
      return $data;



    }

    public function GetQuestionsStep1(Request $request){
      if($request->get("lang") === "fr")
      {
        $headings = DB::table('headings')->select('french as heading','id')->whereIn('id' , [1,2])->get();
        // dd($headings);
      }
      if($request->get("lang") === "it")
      {
        $headings = DB::table('headings')->select('italian as heading','id')->whereIn('id' , [1,2])->get();
      }
      if($request->get("lang") === "si")
      {
        $headings = DB::table('headings')->select('slovene as heading','id')->whereIn('id' , [1,2])->get();
      }
      if($request->get("lang") === "en")
      {
        $headings = DB::table('headings')->select('heading as heading','id')->whereIn('id' , [1,2])->get();
      }
      if($request->get("lang") === "de")
      {
        $headings = DB::table('headings')->select('german as heading','id')->whereIn('id' , [1,2])->get();
      }
        // dd($request->get("lang"));

      foreach ($headings as $heading) {
        if($request->get("lang") === "fr")
        {
         $questions_list = DB::table('french_questions')
         ->where('french_questions.heading_id' , $heading->id)
         ->orderBy("order_no" , 'asc')
         ->get();
       }
       if($request->get("lang") === "it")
       {
         $questions_list = DB::table('italian_questions')
         ->where('italian_questions.heading_id' , $heading->id)
         ->orderBy("order_no" , 'asc')
         ->get();
       }
       if($request->get("lang") === "si")
       {
            // dd("fdfd");
        $questions_list = DB::table('slovene_questions')
        ->where('slovene_questions.heading_id' , $heading->id)
        ->orderBy("order_no" , 'asc')
        ->get();
      }
      if($request->get("lang") === "de")
      {
       $questions_list = DB::table('german_questions')
       ->where('german_questions.heading_id' , $heading->id)
       ->orderBy("order_no" , 'asc')
       ->get();
     }
     if($request->get("lang") === "en")
     {
      $questions_list = DB::table('questions')
      ->where('questions.heading_id' , $heading->id)
      ->orderBy("order_no" , 'asc')
      ->get(); 
    }


    foreach ($questions_list as $value) {
      $answer = DB::table("answers")->where("user_id", $request->get("user"))->where("question_id", $value->id)->first();
      if ($answer) {
        $value->question_id = $answer->question_id;
        $value->user_id = $answer->user_id;
        $value->answer = $answer->answer;
      }else{

        $value->question_id = "";
        $value->user_id = "";
        $value->answer = "";

      }
    }
    if($questions_list)
    {
      $heading->questions = $questions_list;
      $heading->user = $request->get("user");
    }
  }
  return $headings;



}
public function PostQuestionsStep1(Request $request){

        // dd($request->all());
  foreach ($request->get('steponeans') as $value) {
            // dd($value["id"]);
    $anser = DB::table('answers')->where('user_id' , $request->get('userid'))->where('question_id' ,$value["id"])->delete();

    $answers = new Answers;

    $answers->user_id = $request->get('userid');
    $answers->question_id = $value["id"];
    $answers->answer = $value["ans"];
    $answers->save();
  }
  $answers_lists = DB::table('answers')->get();

  $data["status"] = "success";
  $data["answers"] = $answers_lists;
  return $data;



}
public function GetQuestionsStep2(Request $request){

        // dd("fdfd");
 if($request->get("lang") === "fr")
 {
  $headings = DB::table('headings')->select('french as heading','id')->whereIn('id' , [3,4,5])->get();
        // dd($headings);
}
if($request->get("lang") === "it")
{
  $headings = DB::table('headings')->select('italian as heading','id')->whereIn('id' , [3,4,5])->get();
}
if($request->get("lang") === "si")
{
  $headings = DB::table('headings')->select('slovene as heading','id')->whereIn('id' , [3,4,5])->get();
}
if($request->get("lang") === "en")
{
  $headings = DB::table('headings')->select('heading as heading','id')->whereIn('id' , [3,4,5])->get();
}
if($request->get("lang") === "de")
{
  $headings = DB::table('headings')->select('german as heading','id')->whereIn('id' , [3,4,5])->get();
}
  // $headings = DB::table('headings')->whereIn('id' , [3,4,5])->get();
foreach ($headings as $heading) {
 if($request->get("lang") == "fr")
  if($request->get("lang") === "fr")
  {
   $questions_list = DB::table('french_questions')
   ->where('french_questions.heading_id' , $heading->id)
   ->orderBy("order_no" , 'asc')
   ->orderBy("order_no" , 'asc')
   ->get();
 }
 if($request->get("lang") === "it")
 {
   $questions_list = DB::table('italian_questions')
   ->where('italian_questions.heading_id' , $heading->id)
   ->orderBy("order_no" , 'asc')
   ->orderBy("order_no" , 'asc')
   ->get();
 }
 if($request->get("lang") === "si")
 {
   $questions_list = DB::table('slovene_questions')
   ->where('slovene_questions.heading_id' , $heading->id)
   ->orderBy("order_no" , 'asc')
   ->orderBy("order_no" , 'asc')
   ->get();
 }
 if($request->get("lang") === "de")
 {
   $questions_list = DB::table('german_questions')
   ->where('german_questions.heading_id' , $heading->id)
   ->orderBy("order_no" , 'asc')
   ->orderBy("order_no" , 'asc')
   ->get();
 }
 if($request->get("lang") === "en")
 {
  $questions_list = DB::table('questions')
  ->where('questions.heading_id' , $heading->id)
  ->orderBy("order_no" , 'asc')
  ->orderBy("order_no" , 'asc')
  ->get(); 
}
        // $questions_list = DB::table('questions')
        // ->where('questions.heading_id' , $heading->id)
        // ->get();

foreach ($questions_list as $value) {
  $answer = DB::table("answers")->where("user_id", $request->get("user"))->where("question_id", $value->id)->first();
  if ($answer) {
    $value->question_id = $answer->question_id;
    $value->user_id = $answer->user_id;
    $value->answer = $answer->answer;
  }else{

    $value->question_id = "";
    $value->user_id = "";
    $value->answer = "";

  }
}
if($questions_list)
{
  $heading->questions = $questions_list;
  $heading->user = $request->get("user");
}
}
return $headings;
}
public function PostQuestionsStep2(Request $request){

        // dd($request->all());
  foreach ($request->get('steponeans') as $value) {
            // dd($value["id"]);
    $anser = DB::table('answers')->where('user_id' , $request->get('userid'))->where('question_id' ,$value["id"])->delete();

    $answers = new Answers;

    $answers->user_id = $request->get('userid');
    $answers->question_id = $value["id"];
    $answers->answer = $value["ans"];
    $answers->save();
  }
  $answers_lists = DB::table('answers')->get();

  $data["status"] = "success";
  $data["answers"] = $answers_lists;
  return $data;



}
public function GetQuestionsStep3(Request $request){

        // dd("fdfd");
  if($request->get("lang") === "fr")
  {
    $headings = DB::table('headings')->select('french as heading','id')->whereIn('id' , [6,7,8])->get();
        // dd($headings);
  }
  if($request->get("lang") === "it")
  {
    $headings = DB::table('headings')->select('italian as heading','id')->whereIn('id' , [6,7,8])->get();
  }
  if($request->get("lang") === "si")
  {
    $headings = DB::table('headings')->select('slovene as heading','id')->whereIn('id' , [6,7,8])->get();
  }
  if($request->get("lang") === "en")
  {
    $headings = DB::table('headings')->select('heading as heading','id')->whereIn('id' , [6,7,8])->get();
  }
  if($request->get("lang") === "de")
  {
    $headings = DB::table('headings')->select('german as heading','id')->whereIn('id' , [6,7,8])->get();
  }

  // $headings = DB::table('headings')->whereIn('id' , [6,7,8])->get();
  foreach ($headings as $heading) {
    if($request->get("lang") === "fr")
    {
     $questions_list = DB::table('french_questions')
     ->where('french_questions.heading_id' , $heading->id)
     ->orderBy("order_no" , 'asc')
     ->get();
   }
   if($request->get("lang") === "it")
   {
     $questions_list = DB::table('italian_questions')
     ->where('italian_questions.heading_id' , $heading->id)
     ->orderBy("order_no" , 'asc')
     ->get();
   }
   if($request->get("lang") === "si")
   {
     $questions_list = DB::table('slovene_questions')
     ->where('slovene_questions.heading_id' , $heading->id)
     ->orderBy("order_no" , 'asc')
     ->get();
   }
   if($request->get("lang") === "de")
   {
     $questions_list = DB::table('german_questions')
     ->where('german_questions.heading_id' , $heading->id)
     ->orderBy("order_no" , 'asc')
     ->get();
   }
   if($request->get("lang") === "en")
   {
    $questions_list = DB::table('questions')
    ->where('questions.heading_id' , $heading->id)
    ->orderBy("order_no" , 'asc')
    ->get(); 
  }

        // $questions_list = DB::table('questions')
        // ->where('questions.heading_id' , $heading->id)
        // ->get();

  foreach ($questions_list as $value) {
    $answer = DB::table("answers")->where("user_id", $request->get("user"))->where("question_id", $value->id)->first();
    if ($answer) {
      $value->question_id = $answer->question_id;
      $value->user_id = $answer->user_id;
      $value->answer = $answer->answer;
    }else{

      $value->question_id = "";
      $value->user_id = "";
      $value->answer = "";

    }
  }
  if($questions_list)
  {
    $heading->questions = $questions_list;
    $heading->user = $request->get("user");
  }
}
return $headings;



}

public function PostQuestionsStep3(Request $request){

        // dd($request->all());
  foreach ($request->get('steponeans') as $value) {
            // dd($value["id"]);
    $anser = DB::table('answers')->where('user_id' , $request->get('userid'))->where('question_id' ,$value["id"])->delete();

    $answers = new Answers;

    $answers->user_id = $request->get('userid');
    $answers->question_id = $value["id"];
    $answers->answer = $value["ans"];
    $answers->save();
  }
  $answers_lists = DB::table('answers')->get();

  $data["status"] = "success";
  $data["answers"] = $answers_lists;
  return $data;



}

public function commentsSend(Request $request){

// sprintpartners@univ-montp3.fr
  $mymail = $request->get('email');
  $comment = $request->get('msg');
  // Mail::to($mymail)
  // ->cc($mymail)
  // ->bcc($mymail)
  // ->send();
  try {
    Mail::send('email.doc', array('mymail'=>$mymail , 'msg'=>$comment), function($message) use($mymail){
      $message->from('sprintpartners@univ-montp3.fr' ,'Auto assessment');
      $message->to('hemant@theantialias.com')->subject("user comment");
      $message->bcc('sprintpartners@univ-montp3.fr','Auto assessment');
      


    });
  } catch (Exception $e) {

    var_dump($mymail);
  }
}



    //
}
